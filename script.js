//Initializing container for students
let student = []

//Adding Students 
function addStudent(studentName) {
	student.push(studentName);
	console.log(`${studentName} was added to the student's list.`)
}


//Counting Students
function countStudents() {
	if (student.length == 1) {
		console.log(`There are a total of ${student.length} student enrolled.`)
	} else {
		console.log(`There are a total of ${student.length} students enrolled.`)
	}
}

//Printing the Names
function printStudents() {
	// Sort
	student.sort()

	// Loops
	student.forEach(
		function(name) {
			console.log(name)
		}
	)
}


//Finding Student using filter()
function findStudent(studentName) {
	let filteredStudent = student.filter (
			function(name) {
				return name.toLowerCase().includes(studentName)
			}
		)

	if (filteredStudent.length == 1) {
		console.log(`${filteredStudent} is an enrollee`)
	} else if (filteredStudent.length > 1) {
		console.log(`${filteredStudent.join()} are enrollees`)
	} else {
		console.log(`No student found with the name ${studentName}`)
	}
}

//Stretch goals
//Adding Section
function addSection(sectionName) {
	let addedSection = student.map(
			function(name) {
				return (`${name} - section ${sectionName}`)
			}
		)

	console.log(addedSection)
}

//Removing Student
function removeStudent(studentName) {
	let filteredStudent = student.filter (
			function(name) {
				return name.toLowerCase().includes(studentName)
			}
		)


	if (filteredStudent.length == 1) {
		const newStudentName = studentName.charAt(0).toUpperCase() + studentName.slice(1)

		const indexOfStudentName = student.indexOf(newStudentName)

		let newStudentList = student.splice(indexOfStudentName,1)

		console.log(`${newStudentName} was removed from the students list.`)

	} else {
		console.log(`No student found with the name ${studentName}`)
	}
}